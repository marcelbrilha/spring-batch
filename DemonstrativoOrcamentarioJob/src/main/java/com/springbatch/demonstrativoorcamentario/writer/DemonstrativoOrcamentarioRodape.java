package com.springbatch.demonstrativoorcamentario.writer;

import java.io.IOException;
import java.io.Writer;

import org.springframework.batch.item.file.FlatFileFooterCallback;

public class DemonstrativoOrcamentarioRodape implements FlatFileFooterCallback {

	@Override
	public void writeFooter(Writer writer) throws IOException {
		writer.append("Total");
	}
}
