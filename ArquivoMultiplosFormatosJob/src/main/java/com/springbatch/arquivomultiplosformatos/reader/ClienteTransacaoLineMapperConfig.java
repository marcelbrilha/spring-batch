package com.springbatch.arquivomultiplosformatos.reader;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.springbatch.arquivomultiplosformatos.dominio.Cliente;
import com.springbatch.arquivomultiplosformatos.dominio.Transacao;

@Configuration
public class ClienteTransacaoLineMapperConfig {

	@Bean
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PatternMatchingCompositeLineMapper lineMapper() {
		PatternMatchingCompositeLineMapper lineMapper = new PatternMatchingCompositeLineMapper();
		
		lineMapper.setTokenizers(getTokenizers());
		lineMapper.setFieldSetMappers(getFieldSetMappers());
		
		return lineMapper;
	}

	@SuppressWarnings({ "rawtypes" })
	private Map<String, FieldSetMapper> getFieldSetMappers() {
		Map<String, FieldSetMapper> fieldSetMappers = new HashMap<>();
		
		fieldSetMappers.put("0*", fieldSetMappers(Cliente.class));
		fieldSetMappers.put("1*", fieldSetMappers(Transacao.class));
		
		return fieldSetMappers;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private FieldSetMapper fieldSetMappers(Class classe) {
		BeanWrapperFieldSetMapper fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType(classe);
		
		return fieldSetMapper;
	}

	private Map<String, LineTokenizer> getTokenizers() {
		Map<String, LineTokenizer> tokenizers = new HashMap<>();
		
		tokenizers.put("0*", getClienteLineTokenizer());
		tokenizers.put("1*", getTransacaoLineTokenizer());
		
		return tokenizers;
	}

	private LineTokenizer getTransacaoLineTokenizer() {
		DelimitedLineTokenizer lineTokenizers = new DelimitedLineTokenizer();
		
		lineTokenizers.setNames("id", "descricao", "valor");
		lineTokenizers.setIncludedFields(1, 2, 3);
		
		return lineTokenizers;
	}

	private LineTokenizer getClienteLineTokenizer() {
		DelimitedLineTokenizer lineTokenizers = new DelimitedLineTokenizer();
		
		lineTokenizers.setNames("nome", "sobrenome", "idade", "email");
		lineTokenizers.setIncludedFields(1, 2, 3, 4);
		
		return lineTokenizers;
	}
}
