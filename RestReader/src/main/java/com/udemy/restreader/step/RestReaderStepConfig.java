package com.udemy.restreader.step;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.udemy.restreader.DTO.EnderecoDTO;

@Configuration
public class RestReaderStepConfig {

	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	@Bean
	public Step restReaderStep(ItemReader<EnderecoDTO> restReader, ItemWriter<EnderecoDTO> restWriter) {
		return stepBuilderFactory
				.get("restReaderStepConfig")
				.<EnderecoDTO,EnderecoDTO>chunk(1)
				.reader(restReader)
				.writer(restWriter)
				.build();
	}
}
