package com.udemy.restreader.job.writer;

import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.udemy.restreader.DTO.EnderecoDTO;

@Configuration
public class RestReaderWriterConfig {

	@Bean
	public ItemWriter<EnderecoDTO> restWriter() {
		return enderecos -> enderecos.forEach(System.out::println);
	}
}
