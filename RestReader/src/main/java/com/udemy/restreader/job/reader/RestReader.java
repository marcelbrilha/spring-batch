package com.udemy.restreader.job.reader;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.udemy.restreader.DTO.EnderecoDTO;

@Configuration
public class RestReader implements ItemReader<EnderecoDTO>{
	
	private final String apiUrl = "https://viacep.com.br/ws/01001000/json/";
	
    private EnderecoDTO endereco;

	@Override
	public EnderecoDTO read() throws Exception {
		if (endereco == null) {
			endereco = fetchStudentDataFromAPI();

			return endereco;
        }
 
        return null;
	}

	private EnderecoDTO fetchStudentDataFromAPI() {
        ResponseEntity<EnderecoDTO> response = new RestTemplate().getForEntity(
        	apiUrl,
            EnderecoDTO.class
        );

        return response.getBody();
    }
}
