package com.udemy.restreader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestReaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestReaderApplication.class, args);
	}

}
